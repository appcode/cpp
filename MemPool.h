#ifndef __H_MEMPOOL_
#define __H_MEMPOOL__

#include <mutex>

//#pragma pack(1)

#define ALIGN 8
#define align(a,n) \
    ((a + n - 1) & (~(n - 1)))

//4KB以下的内存分配使用内存池
#define MAX_MEM (4 * 1024)
#define MAX_HEAD (MAX_MEM >> 3)

struct _BlockHead;
struct _MemBlock;

typedef _BlockHead BlockHead;
typedef _MemBlock MemBlock;

struct _BlockHead {
    int m_free_count;
    MemBlock* m_free_list;
    std::mutex _mutex;
};

struct _MemBlock {
    MemBlock* m_next;
    BlockHead* m_head;
    char m_data[0];
};

class MemPool {

public:
    static MemPool* getInstance();
    void* _malloc(int n);
    void* _calloc(int n);
    void _free(void* p);

private:
    MemPool();
    ~MemPool();

    BlockHead m_block_head[MAX_HEAD];
};

//static struct helper{
//    helper(){
//        MemPool* pool = MemPool::getInstance();
//    }
//    ~helper(){

//    }
//} _obj;

#endif
