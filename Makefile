CXX = g++
#CXX = clang++
CXXFLAGS += -O0 -g -Wall -Wextra -std=c++11
#CXXFLAGS += -g -Wall -Wextra -std=c++11
CXXSHARED += -shared -fPIC

#################################################################################################
# only modify below usually

TARGETS = mem_pool_test 

LIBS += -L/lib64 -lprofiler -lunwind -lpthread

SRC = $(wildcard *.cpp)
INC = $(wildcard *.h)
DIR = $(notdir $(SRC))
OBJ = $(patsubst %.cpp,%.o,$(DIR))

#################################################################################################




#################################################################################################
# do not modify below
#################################################################################################

CLEANUP = rm -rf $(OBJ) $(TARGETS) core*

all:$(TARGETS)
	

clean:
	$(CLEANUP)

$(TARGETS):$(OBJ)
	$(CXX) $(CXXFLAGS) $(LIBS)  $^ -o $@

.cpp.o:
	$(CXX) -c $(CXXFLAGS) -o $@ $^
