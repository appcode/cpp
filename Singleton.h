#ifndef SINGLETON_H
#define SINGLETON_H

template<class T>
class Singleton
{
public:
    static T* getInstance(){
        static T instance;
        return &instance;
    }

protected:
    Singleton(){}
    virtual ~Singleton(){}

    Singleton(const Singleton<T>&);
    Singleton<T>& operator=(const Singleton<T>&);
};

#endif // SINGLETON_H
